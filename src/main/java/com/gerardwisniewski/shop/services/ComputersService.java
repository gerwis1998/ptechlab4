package com.gerardwisniewski.shop.services;

import com.gerardwisniewski.shop.model.Computer;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ComputersService extends EntityService<Computer> {

    public ComputersService(EntityManager em) {

        super(em, Computer.class, Computer::getId);
    }

    public List<Computer> findAll() {
        return em.createNamedQuery(Computer.FIND_ALL, Computer.class).getResultList();
    }

}
