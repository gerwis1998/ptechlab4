package com.gerardwisniewski.shop.services;

import com.gerardwisniewski.shop.model.Computer;
import com.gerardwisniewski.shop.model.Package;
import com.gerardwisniewski.shop.model.Order;
import com.gerardwisniewski.shop.services.exceptions.InvalidRequestException;
import com.gerardwisniewski.shop.services.exceptions.OrderEmptyException;
import com.gerardwisniewski.shop.services.exceptions.OutOfStockException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class OrdersService extends EntityService<Order> {

    public OrdersService(EntityManager em) {

        super(em, Order.class, Order::getId);
    }

    public List<Order> findAll() {
        return em.createNamedQuery(Order.findAll, Order.class).getResultList();
    }

    @Transactional
    public void placeOrder(Order order) {
        if(order == null){
            throw new OrderEmptyException();
        }
        else if(order.getComputers().size() < 1){
            throw new InvalidRequestException();
        }
        for (Package computerStub : order.getComputers()) {
            if(computerStub.getComputer() == null){
                throw new InvalidRequestException();
            }
            if(computerStub.getComputer().getPrice() != null){
                throw new InvalidRequestException();
            }
            if(!computerStub.getComputer().getType().equalsIgnoreCase("pc")){
                if(!computerStub.getComputer().getType().equalsIgnoreCase("laptop")){
                    throw new InvalidRequestException();
                }
            }
            Computer computer = em.find(Computer.class, computerStub.getComputer().getId());
            if(computer == null){
                throw new InvalidRequestException();
            }
            if (computerStub.getQuantity() < 2 && order.getComputers().size() == 1) {
                throw new InvalidRequestException();
            }
            if (computer.getAmount() < computerStub.getQuantity()) {
                throw new OutOfStockException();
            } else {
                int newAmount = computer.getAmount() - computerStub.getQuantity();
                computer.setAmount(newAmount);
            }
        }
        save(order);
    }
}
