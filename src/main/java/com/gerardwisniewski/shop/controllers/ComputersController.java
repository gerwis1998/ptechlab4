package com.gerardwisniewski.shop.controllers;

import com.gerardwisniewski.shop.model.Computer;
import com.gerardwisniewski.shop.services.ComputersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CONFLICT;


@RestController
@RequestMapping("/computers")
public class ComputersController {

    private final ComputersService computersService;

    public ComputersController(ComputersService computersService) {
        this.computersService = computersService;
    }

    @GetMapping
    public List<Computer> listComputers() {
        return computersService.findAll();
    }

    @PostMapping
    public ResponseEntity<Void> addComputer(@RequestBody Computer computer, UriComponentsBuilder uriBuilder) {

        if (computersService.find(computer.getId()) == null) {
            computersService.save(computer);

            URI location = uriBuilder.path("/computers/{id}").buildAndExpand(computer.getId()).toUri();
            return ResponseEntity.created(location).build();

        } else {
            return ResponseEntity.status(CONFLICT).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Computer> getComputer(@PathVariable UUID id) {
        Computer computer = computersService.find(id);
        return computer != null ? ResponseEntity.ok(computer) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateComputer(@RequestBody Computer computer, @PathVariable UUID id) {
        if (computersService.find(id) != null) {
            computer.setId(id);
            computersService.save(computer);
            return ResponseEntity.ok().build();

        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
