package com.gerardwisniewski.shop.controllers;

import com.gerardwisniewski.shop.model.Computer;
import com.gerardwisniewski.shop.model.Package;
import com.gerardwisniewski.shop.model.Order;
import com.gerardwisniewski.shop.services.OrdersService;
import com.gerardwisniewski.shop.services.exceptions.OutOfStockException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@RestController
public class ShopController {

    private final OrdersService ordersService;

    public ShopController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }


    @GetMapping("/orders")
    public List<Order> listOrders() {
        return ordersService.findAll();
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable UUID id) {
        Order order = ordersService.find(id);
        return isNull(order) ? ResponseEntity.notFound().build() : ResponseEntity.ok(order);
    }


    @PostMapping("/orders")
    public ResponseEntity<String> addOrder(@RequestBody Order order, UriComponentsBuilder uriBuilder) {
        if(order.getComputers().size() == 1){
            Package pckg = order.getComputers().get(0);
            if(pckg.getQuantity() < 2){
                return new ResponseEntity<>("Minimal order of 2 computers", HttpStatus.BAD_REQUEST);
            }
        }
        else if(order.getComputers().size() < 2) {
            return new ResponseEntity<>("Minimal order of 2 computers", HttpStatus.BAD_REQUEST);
        }
        try {
            ordersService.placeOrder(order);
            URI location = uriBuilder.path("/orders/{id}").buildAndExpand(order.getId()).toUri();
            return ResponseEntity.created(location).build();

        } catch (OutOfStockException e) {
            return new ResponseEntity<>("Out of stock", HttpStatus.BAD_REQUEST);
        }
    }
}
