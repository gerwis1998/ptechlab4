package com.gerardwisniewski.shop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "package")
@EqualsAndHashCode(of = "id")
public class Package {
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    int quantity;


    @Getter
    @Setter
    @ManyToOne
    Computer computer;
}