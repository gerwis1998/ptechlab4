package com.gerardwisniewski.shop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@EqualsAndHashCode(of = "id")
@NamedQueries(value = {
        @NamedQuery(name = Computer.FIND_ALL, query = "SELECT b FROM Computer b")
})
public class Computer {
    public static final String FIND_ALL = "Computer.FIND_ALL";

    @Setter
    @Getter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    String type;

    @Getter
    @Setter
    String processor;

    @Getter
    @Setter
    String graphicCard;

    @Getter
    @Setter
    String memory;

    @Getter
    @Setter
    Integer amount;

    @Getter
    @Setter
    Integer price;
}
