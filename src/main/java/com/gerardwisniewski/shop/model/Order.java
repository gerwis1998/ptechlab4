package com.gerardwisniewski.shop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static javax.persistence.TemporalType.TIMESTAMP;

@Entity
@Table(name = "orders")
@EqualsAndHashCode(of = "id")
@NamedQueries(value = {
        @NamedQuery(name = Order.findAll, query = "SELECT o FROM Order o")
})
public class Order {
    public static final String findAll = "Orders.findAll";

    @Getter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @OneToMany(cascade = CascadeType.ALL)
    List<Package> computers = new ArrayList<>();

    @Getter
    @Temporal(TIMESTAMP)
    Date creationDate;

    @PrePersist
    public void prePersist(){
        this.creationDate = new Date();
    }
}
