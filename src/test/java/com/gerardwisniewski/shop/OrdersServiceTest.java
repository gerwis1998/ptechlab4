package com.gerardwisniewski.shop;

import com.gerardwisniewski.shop.model.Computer;
import com.gerardwisniewski.shop.model.Order;
import com.gerardwisniewski.shop.model.Package;
import com.gerardwisniewski.shop.services.OrdersService;
import com.gerardwisniewski.shop.services.exceptions.InvalidRequestException;
import com.gerardwisniewski.shop.services.exceptions.OrderEmptyException;
import com.gerardwisniewski.shop.services.exceptions.OutOfStockException;
import org.aspectj.weaver.ast.Or;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

	@Mock
	EntityManager em;

	@Test(expected = OutOfStockException.class)
	public void whenOrderedPCNotAvailable_placeOrderThrowsInvalidRequestException() {
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Computer computer = new Computer();
		computer.setAmount(0);
		computer.setType("pc");
		pckg.setComputer(computer);
		pckg.setQuantity(2);
		order.getComputers().add(pckg);
		OrdersService ordersService = new OrdersService(em);
		Mockito.when(em.find(Computer.class, computer.getId())).thenReturn(computer);
		//Act
		ordersService.placeOrder(order);
		//Assert - exception expected
	}

	@Test
	public void whenOrderedTwoPCsAvailable_placeOrderDecreasesAmountByTwo() {
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Computer computer = new Computer();
		computer.setAmount(2);
		computer.setType("pc");
		pckg.setComputer(computer);
		pckg.setQuantity(2);
		order.getComputers().add(pckg);
		Mockito.when(em.find(Computer.class, computer.getId())).thenReturn(computer);
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert
		assertEquals(0, (int) computer.getAmount());
		Mockito.verify(em, times(1)).persist(order);
	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedOnePC_placeOrderThrowsInvalidRequestException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Computer computer = new Computer();
		computer.setAmount(1);
		computer.setType("pc");
		pckg.setComputer(computer);
		pckg.setQuantity(1);
		order.getComputers().add(pckg);
		OrdersService ordersService = new OrdersService(em);
		Mockito.when(em.find(Computer.class, computer.getId())).thenReturn(computer);
		//Act
		ordersService.placeOrder(order);
		//Assert - exception expected
	}

	@Test(expected = OrderEmptyException.class)
	public void whenOrderNull_placeOrderThrowsOrderEmptyException(){
		//Arrange
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(null);
		//Assert - exception expected
	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedNothing_placeOrderThrowsInvalidRequestException(){
		//Arrange
		Order order = new Order();
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert - exception expected
	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedPCDontExists_placeOrderThrowsInvalidRequestException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		pckg.setComputer(null);
		pckg.setQuantity(2);
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert - exception expected

	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedTwoNullPcs_placeOrderThrowsInvalidRequestException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Package pckg1 = new Package();
		pckg.setComputer(null);
		pckg1.setComputer(null);
		order.getComputers().add(pckg1);
		order.getComputers().add(pckg);
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert - expected exception
	}

	@Test(expected = OutOfStockException.class)
	public void whenOrderedTwoPCsAndOneIsAvailable_placeOrderThrowsOutOfStockException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Package pckg1 = new Package();
		Computer computer = new Computer();
		computer.setAmount(1);
		computer.setType("pc");
		Computer computer1 = new Computer();
		computer1.setAmount(0);
		computer1.setType("laptop");
		pckg.setComputer(computer);
		pckg.setQuantity(1);
		pckg1.setComputer(computer1);
		pckg1.setQuantity(1);
		order.getComputers().add(pckg);
		order.getComputers().add(pckg1);
		OrdersService ordersService = new OrdersService(em);
		Mockito.when(em.find(Computer.class, computer.getId())).thenReturn(computer);
		Mockito.when(em.find(Computer.class, computer1.getId())).thenReturn(computer1);
		//Act
		ordersService.placeOrder(order);
		//Assert - expected exception
	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedPCsWithSetPrice_placeOrderThrowsInvalidOrderException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Computer computer = new Computer();
		computer.setAmount(2);
		computer.setPrice(1);
		pckg.setComputer(computer);
		pckg.setQuantity(2);
		order.getComputers().add(pckg);
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert - expected exception
	}

	@Test(expected = InvalidRequestException.class)
	public void whenOrderedPCsWithInvalidType_placeOrderThrowsInvalidOrderException(){
		//Arrange
		Order order = new Order();
		Package pckg = new Package();
		Computer computer = new Computer();
		computer.setType("2137");
		computer.setAmount(2);
		pckg.setComputer(computer);
		pckg.setQuantity(2);
		order.getComputers().add(pckg);
		OrdersService ordersService = new OrdersService(em);
		//Act
		ordersService.placeOrder(order);
		//Assert - expected exception
	}
}
